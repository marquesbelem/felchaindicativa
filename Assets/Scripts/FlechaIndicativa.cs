﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlechaIndicativa : MonoBehaviour
{
    [SerializeField]
    Transform target; //Objetivo
    Vector3 targetPoint;
    Quaternion targetRotation;

    void Start()
    {
        //Colocar a seta no top da camera
        float size = Camera.main.orthographicSize;
        this.transform.position = new Vector3(this.transform.position.x, size, this.transform.position.z);
    }

    void Update()
    {
        targetPoint = new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z) - transform.position;
        targetRotation = Quaternion.LookRotation(-targetPoint, Vector3.up);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 2.0f);
    }
}
